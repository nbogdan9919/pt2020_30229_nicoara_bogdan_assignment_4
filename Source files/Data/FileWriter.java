package Data;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileWriter {


    public void generateBillTxt(int id,String s)
    {
        PrintWriter pw;
        try {
            File iesire = new File("Bill"+id+".txt");
            if (!iesire.exists()) {
                iesire.createNewFile();
            }

            pw=new PrintWriter(iesire);
            pw.write(s);
            pw.close();
        }
        catch(IOException e){}

    }
}
