package Data;
import Business.*;
import Business.Restaurant;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RestaurantSerializator {

    public RestaurantSerializator()
    {

    }


    public void serialize(Object obj) throws Exception
    {
        FileOutputStream fout=new FileOutputStream("restaurant.ser");
        ObjectOutputStream out=new ObjectOutputStream(fout);
        out.writeObject(obj);
        out.flush();
    }

    public Restaurant deserialize(String s)
    {
        Restaurant a=new Restaurant();
        try {
            ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(s));
            a= (Restaurant) objIn.readObject();

            objIn.close();
        } catch (Exception ex) {
        }
        return a;

    }


}
