package Business;
import java.util.LinkedList;

public class BaseProduct extends MenuItem implements java.io.Serializable{

    private String nume;
    private float pret;
    public BaseProduct(){

    }
    public BaseProduct(String nume,float pret)
    {
        this.pret=pret;
        this.nume=nume;

    }
    public LinkedList<MenuItem> getItems()
    {
        LinkedList<MenuItem> items=new LinkedList<>();
        items.add(this);
        return items;

    }


    public String getNume() {
        return nume;
    }

    public void setItems(LinkedList<MenuItem> e)
    {}

    public void setNume(String nume) {
        this.nume = nume;
    }

    public float getPret() {
        return pret;
    }

    public void setPret(float pret) {
        this.pret = pret;
    }

    public float computePrice()
    {
        return this.pret;
    }

    public String toString()
    {
        String s="";
        s=s+nume+" "+pret;

        return s;
    }
    public void add(MenuItem e){

    }
}