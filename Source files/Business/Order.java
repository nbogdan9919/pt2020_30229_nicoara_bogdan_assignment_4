package Business;
import java.util.Calendar;
import java.util.Date;
import java.util.*;
import java.util.HashMap;
public class Order implements java.io.Serializable{

    private int OrderID;
    private static int nrOrderID=0;
    private Date Date;
    private int Table;

    public Order(int Table)
    {
        nrOrderID++;
        OrderID=nrOrderID;
        this.Table=Table;
        Date=Calendar.getInstance().getTime();
    }
    public int getOrderID()
    {
        return OrderID;
    }
    public int getTable()
    {
        return Table;
    }

    public void setOrderID(int id)
    {
        this.OrderID=id;
    }

    @Override
    public int hashCode()
    {
        int hash=23;

        hash=31*hash+OrderID;
        hash=31*hash+Table;

        return hash;
    }

    public String toString()
    {
        String s="";
        s=s+"\nOrder id: "+OrderID+"\nTable: "+Table+"\nDate: "+Date+" \n\nProduse comandate:";
        return s;
    }


}
