package Business;
import java.io.*;
import java.util.*;


public class Restaurant  implements Serializable, IRestaurantProcessing {


    private  Map<Order, LinkedList<MenuItem>> orders=new HashMap<Order,LinkedList<MenuItem>>();
    private  LinkedList<MenuItem> meniu=new LinkedList<>();

    public Restaurant() {

    }

    @Override
    public  Map<Order,LinkedList<MenuItem>> getOrders()
    {
        return orders;
    }

    /**
     * adauga un item in meniu
     * @param item ce item sa adauge in meniu
     */
    @Override
    public void addMenuItem(MenuItem item)
    {
        meniu.add(item);
    }

    /**
     * sterge un item din meniu
     * @param item ce item sa stearga din meniu
     */
    @Override
    public void removeMenuItem(MenuItem item)
    {
        meniu.remove(item);
    }

    /**
     * Insereaza o comanda in map
     * @param a un order
     * @param items iteme pentru acel order
     */
    @Override
    public void addOrder(Order a, LinkedList<MenuItem> items)
    {
        orders.put(a,items);


    }

    /**
     * returneaza meniul restaurantului
     * @return elementele din meniul restaurantului
     */
    @Override
    public  LinkedList<MenuItem> getMeniu()
    {
        return meniu;
    }

    /**
     *
     * @param actualName numele produsului care trebuie schimbat
     * @param newName noul nume pentru produs
     * @param newPrice noul pret pentru produs(doar daca e base product) altfel se calculeaza automat
     * @param newElements noua lista de componente pt acest produs
     */
    @Override
    public void editItem( String actualName,String newName,float newPrice,LinkedList<MenuItem> newElements)
    {int i=0;

        for (MenuItem m : this.getMeniu()) {

            if (m.getNume().equals(actualName)) {
                m.setNume(newName);
                m.setPret(newPrice);
                m.setItems(newElements);
                this.getMeniu().set(i, m); }
            i++;
        }

    }






}

