package Business;
import java.util.LinkedList;
import java.util.Map;

public interface IRestaurantProcessing {


    void addOrder(Order a, LinkedList<MenuItem> items);

    LinkedList<MenuItem> getMeniu();
    Map<Order, LinkedList<MenuItem>> getOrders() ;

    void addMenuItem(MenuItem item);
    void removeMenuItem(MenuItem item);
    public void editItem( String actualName,String newName,float newPrice,LinkedList<MenuItem> newElements);

}
