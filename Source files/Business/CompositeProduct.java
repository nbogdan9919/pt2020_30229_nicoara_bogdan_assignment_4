package Business;
import java.util.LinkedList;

public class CompositeProduct extends MenuItem implements java.io.Serializable{


    private String nume;
    private LinkedList<MenuItem> items=new LinkedList<MenuItem>();
    public CompositeProduct(){


    }
    public CompositeProduct(String nume)
    {
        this.nume=nume;
    }

    @Override
    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public LinkedList<MenuItem> getItems() {
        return items;
    }



    public void setItems(LinkedList<MenuItem> items) {
        this.items = items;
    }

    public void setPret(float m)
    {}

    public void add(MenuItem e)
    {
        BaseProduct pr=new BaseProduct();
        CompositeProduct comp=new CompositeProduct();

        if(e.getClass()==pr.getClass())
        {
            add((BaseProduct)e);

        }
        else add((CompositeProduct)e);

    }
    public void add(BaseProduct e)
    {
        items.add(e);
    }

    public void add(CompositeProduct e)
    {
        LinkedList<MenuItem> itemeProdus=new LinkedList<MenuItem>();
        itemeProdus=e.getItems();

        for(MenuItem pr:itemeProdus)
        {
            items.add(pr);
        }

    }

    public void delete(MenuItem a)
    {
        items.remove(a);

    }

    public float computePrice()
    {
        float pret=0;
        for(MenuItem e:items)
        {
            pret=pret+e.computePrice();
        }
        return pret;
    }

    public String toString()
    {
        String s="";
        s=s+nume;
        for(MenuItem e:items)
        {
            s=s+e.toString();
        }
        return s;
    }


}
