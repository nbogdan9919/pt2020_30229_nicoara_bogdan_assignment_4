package Presentation;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import Business.*;
import Business.MenuItem;

public class WaiterView {

    private JFrame frame;
    private JPanel panel;
    private JButton addOrder=new JButton("add new order");
    private JButton viewOrders=new JButton("view orders");
    private JButton bill=new JButton("generate bill");
    private WaiterController controller=new WaiterController(this);

    private JTextField tabelNumber=new JTextField(35);
    private DefaultListModel<MenuItem> model=new DefaultListModel<>();
    private DefaultListModel<MenuItem> model2=new DefaultListModel<>();

    private JTextField billNumber=new JTextField(35);
    private JFrame frameOrder;
    private Restaurant restaurant;

    public WaiterView(String nume, Restaurant a)
    {this.restaurant=a;
        controller =new WaiterController(this,a);
        frame=new JFrame(nume);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(400,400);

        panel=new JPanel();
        panel.add(addOrder);
        panel.add(viewOrders);
        panel.add(bill);

        frame.add(panel);
        frame.setVisible(true);

        addOrder.addActionListener(controller.new ButonAddOrder());
        viewOrders.addActionListener(controller.new ButonView());
        bill.addActionListener(controller.new ButonBill());

    }

    public LinkedList<MenuItem> getMeniu()
    {
        return controller.getMeniu();
    }
    public JFrame getFrame()
    {
        return frame;
    }

    /**
     *
     * @return frame pentru butonul de add order
     */
    public JFrame frameButonAddOrder(){

        JFrame frame = new JFrame("buton add order");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setLayout(new GridLayout());

        JPanel panel=panelButonAddOrder();

        frame.add(panel);
        frame.setVisible(true);
        return frame;

    }

    /**
     *
     * @return panel pentru butonul de add order
     */
    public JPanel panelButonAddOrder(){

        JPanel panel=new JPanel();
        LinkedList<MenuItem> meniu=controller.getMeniu();
        JList<MenuItem> stanga=new JList<>();
        JList<MenuItem>dreapta=new JList<>();
        model=new DefaultListModel<>();

        stanga.setModel(model);
        dreapta.setModel(model2);
        for(MenuItem m:meniu)
            model.addElement(m);

        JButton move=new JButton("move-->");
        JButton insert=new JButton("Create Order");
        insert.addActionListener(controller.new ButonInsertOrder());

        move.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                MenuItem m=stanga.getSelectedValue();
                if(m!=null)
                    model2.addElement(m);

                m=dreapta.getSelectedValue();
                if(m!=null)
                    model2.removeElement(dreapta.getSelectedValue());

            }});

        panel.add(stanga);
        panel.add(move);
        panel.add(dreapta);
        panel.add(new JLabel("numar masa:"));
        panel.add(tabelNumber);
        panel.add(insert);return panel;
    }

    public String getNumarMasa()
    {
        String text=tabelNumber.getText();

        return text;
    }

    /**
     *
     * @return o lista cu elementele din lista creeata de elemnentele inserate prin butonul move
     */
    public LinkedList<MenuItem> getElementeDreapta() {

        LinkedList<MenuItem> m = new LinkedList<>();

        for (int i = 0; i < model2.size(); i++) {
            m.add(model2.getElementAt(i));
        }
        model2 = new DefaultListModel<>();
        return m;
    }

    /**
     *
     * @return textul din fereastra de bill
     */
    public String getOrderIDFromView()
    {
        String text=billNumber.getText();

        return text;

    }

    /**
     *
     * @return frame pentru butonul de compute bill
     */
    public JFrame framePtButonComputeBill()
    {
        JFrame frame=new JFrame("compute bill");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setLayout(new GridLayout());
        JButton generateBill=new JButton("generate bill");
        generateBill.addActionListener(controller.new ButonGenerateBill());

        JPanel panel=new JPanel();
        panel.add(new JLabel("Order id pt care sa se genereze factura:"));
        panel.add(billNumber);
        panel.add(generateBill);
        frame.add(panel);
        frame.setVisible(true);

        return frame;

    }

}
