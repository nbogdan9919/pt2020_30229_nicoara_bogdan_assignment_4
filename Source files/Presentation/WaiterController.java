package Presentation;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import Business.*;
import Data.FileWriter;

public class WaiterController {


    private WaiterView view;
    private JFrame frameAddOrder;
    private IRestaurantProcessing restaurant;

    public WaiterController(WaiterView a)
    {
        this.view=a;
    }
    public WaiterController(WaiterView view, Restaurant a)
    {
        this.view=view;
        this.restaurant=a;

    }
    public void eroareCitire(String mesaj)
    {
        JOptionPane.showMessageDialog(view.getFrame(), mesaj);
    }

    public LinkedList<MenuItem> getMeniu()
    {
        return restaurant.getMeniu();
    }

    /**
     * butonul de add order din waiter
     */
    class ButonAddOrder implements ActionListener{

        public void actionPerformed(ActionEvent e)
        {
            frameAddOrder=view.frameButonAddOrder();

        }
    }

    /**
     *
     * @param text  reprezinta un string citit dintr-un field
     * @return 1 daca text e numar,0 daca nu
     */
    public int textENumar(String text)
    {
        for(char c:text.toCharArray())
        {
            if(!Character.isDigit(c))
            {
                return 0;
            }
        }
        return 1;
    }

    /**
     * butonul de insert din add order
     */
    class ButonInsertOrder implements ActionListener{
        /**
         * metoda pt calcul id maxim din orders
         * @return id ultimei comenzi din restaurant(comanda cu id maxim+1)
         */
        public int getIDUltimeiComezi()
        {
            int id=0;
            Map<Order, LinkedList<MenuItem>> comenzi=new HashMap<Order,LinkedList<MenuItem>>();
            comenzi=restaurant.getOrders();

            for(Map.Entry<Order, LinkedList<MenuItem>> orders: comenzi.entrySet())
            {
                int current=orders.getKey().getOrderID();
                if(id<current)
                {
                    id=current;
                }

            }
            return id+1;
        }

        public void actionPerformed(ActionEvent e)
        {
            LinkedList<MenuItem> items = new LinkedList<MenuItem>();
            items = view.getElementeDreapta();
            if(view.getNumarMasa().isEmpty()||textENumar(view.getNumarMasa())==0)
            {
                eroareCitire("introduceti un NUMAR pentru masa, nu este admis text");
            }
            else {
                if(items.size()==0){
                    eroareCitire("Introduceti cel putin 1 produs, nu se pot creea comenzi goale");
                }
                else {
                    Order a = new Order(Integer.parseInt(view.getNumarMasa()));
                    a.setOrderID(getIDUltimeiComezi());

                    restaurant.addOrder(a, items);
                }
            }
            frameAddOrder.dispose();
            frameAddOrder=view.frameButonAddOrder();



        }

    }

    /**
     *
     * @param e o lista de produse
     * @return pretul total pentru aceasta lista
     */
    public float computePrice(LinkedList<MenuItem> e)
    {
        float pret=0;

        for(MenuItem m:e)
        {
            pret=pret+m.computePrice();
        }

        return pret;
    }

    /**
     * butonul de view din waiter
     */
    class ButonView implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            JFrame f = new JFrame("view orders");
            Map<Order, LinkedList<MenuItem>> comenzi=new HashMap<Order,LinkedList<MenuItem>>();
            comenzi=restaurant.getOrders();

            String data[][] = new String[comenzi.size()][100];
            int i = 0;

            for(Map.Entry<Order, LinkedList<MenuItem>> orders: comenzi.entrySet())
            {
                data[i]= new String[]{String.valueOf(orders.getKey().getOrderID()),String.valueOf(orders.getKey().getTable()),String.valueOf(computePrice(orders.getValue()))};
                i++;

            }
            String column[]={"order","masa","pret"};
            JTable tabel=new JTable(data,column);

            JScrollPane sp=new JScrollPane(tabel);
            f.add(sp);
            f.setSize(200,300);
            f.setVisible(true);
        }

    }

    /**
     * Butonul de compute bill din waiter
     */
    class ButonBill implements ActionListener{

        public void actionPerformed(ActionEvent e)
        {
            JFrame frame=view.framePtButonComputeBill();

        }
    }

    /**
     *
     * @param orderID id unui order
     * @return un text cu toate informatiile despre un order
     */
    public String BillText(int orderID)
    {
        String s="";
        Map<Order, LinkedList<MenuItem>> comenzi=new HashMap<Order,LinkedList<MenuItem>>();
        comenzi=restaurant.getOrders();

        for(Map.Entry<Order, LinkedList<MenuItem>> orders: comenzi.entrySet())
        {
            if(orders.getKey().getOrderID()==orderID)
            {
                s=s+orders.getKey().toString()+" ";
                for(MenuItem item:orders.getValue())
                {
                    s=s+"\n"+item.getNume()+" "+String.valueOf(item.computePrice())+" ";

                }
                s=s+"\n\n"+"Total: "+computePrice(orders.getValue());
            }
        }
        return s;

    }


/**
 * butonul de generate din fereastra de bill
 */
    class ButonGenerateBill implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            if(view.getOrderIDFromView().isEmpty()||textENumar(view.getNumarMasa())==0)
            {
                eroareCitire("introduceti un NUMAR pentru order");
            }
            int OrderID=Integer.parseInt(view.getOrderIDFromView());
            String s=BillText(OrderID);
            if(s.isEmpty())
            {
                eroareCitire("OrderID introdus nu exista/nu este valid");
            }
            else{
                new FileWriter().generateBillTxt(OrderID,s);
            }
        }
    }
}
