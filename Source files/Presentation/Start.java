package Presentation;

/**
 * clasa principala a programului
 */
public class Start {


    public static void main(String[] args) throws Exception{

String s;

        if(args.length!=1)
        {
            s="";
            System.out.println("Nu s-a incarcat restaurantul din vreun fisierul, restaurantul este gol");
        }
        else{
            s=args[0];
            System.out.println("S-a incarcat restaurantul din fisierul "+s);

        }

        MainUI main=new MainUI(s);

    }


}
