package Presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import Business.*;
import Business.MenuItem;

/**
 *Clasa cu toate utilitatiile de la toate butoanele din Admin
 */
public class AdministratorController  {

    private JFrame frameCompositeProduct;
    private JFrame frameEdit;
    private JFrame frameDelete;


    private AdministratorView view;
    private IRestaurantProcessing restaurant;

    public AdministratorController(AdministratorView a)
    {
        this.view=a;
    }
    public AdministratorController(AdministratorView a, Restaurant r)
    {
        this.view=a;
        this.restaurant=r;

    }

    /**
     *
     * @return meniul din restaurant
     */
    public LinkedList<MenuItem> getMeniu()
    {
        return restaurant.getMeniu();
    }

    public void eroareCitire(String mesaj)
    {
        JOptionPane.showMessageDialog(view.getFrame(), mesaj);
    }

    /**
     * fereastra de base product
     */
    class butonBaseProduct implements ActionListener {

        public void actionPerformed(ActionEvent e) {


            JFrame frame = view.butonBaseProduct();

        }
    }

    /**
     * butonul insert din Base Product
     */
    class butonInsereazaBaseProduct implements ActionListener{

        public void actionPerformed(ActionEvent e)
        {
            BaseProduct newBaseProduct=view.getBaseProduct();
            int ok=1;
            if(newBaseProduct.getNume()=="Eroare")
                eroareCitire("Introduceti numele SI pretul produsului");

            else {

                for(MenuItem m:restaurant.getMeniu())
                {
                    if(m.getNume().equals(newBaseProduct.getNume()))
                    {
                        eroareCitire("Acest produs exista deja");
                        ok=0;
                    }
                }
                if(ok==1)
                {
                    restaurant.addMenuItem(newBaseProduct);
                }
            }

        }
    }


    /**
     * Butonul View din Admin
     */
    class butonView implements ActionListener{

        public void actionPerformed(ActionEvent e)
        {
            JFrame f=new JFrame();

            String data[][]=new String[restaurant.getMeniu().size()][100];
            int i=0;
            for(MenuItem m:restaurant.getMeniu()) {
                data[i] = (new String[]{m.getNume(), String.valueOf(m.computePrice())});
                i++;
            }

            String column[]={"nume","pret"};

            JTable tabel=new JTable(data,column);

            JScrollPane sp=new JScrollPane(tabel);
            f.add(sp);
            f.setSize(200,300);
            f.setVisible(true);

        }

    }

    /**
     * Butonul pentru fereastra de add Composite product
     */
    class butonCompositeProduct implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            frameCompositeProduct = view.butonCompositeProduct();
        }
    }

    /**
     *
     * butonul de Insert din composite product
     */

    class ButonInsereazaCompositeItem implements ActionListener{

        private DefaultListModel<MenuItem> model;
        DefaultListModel<MenuItem> model2;

        public ButonInsereazaCompositeItem(DefaultListModel<MenuItem> model, DefaultListModel<MenuItem> model2)
        {
            this.model=model;
            this.model2=model2;
        }
        public void actionPerformed(ActionEvent e) {

            CompositeProduct newItem = new CompositeProduct(view.getNumeCompositeProduct());
            if (newItem.getNume().length()==0) {
                eroareCitire("Produsul trebuie sa aiba un nume");

            }
            else {
                LinkedList<MenuItem> elementePtProdusNou = new LinkedList<>();
                elementePtProdusNou = view.getElementeDreapta();

                for (MenuItem m : elementePtProdusNou) {
                    newItem.add(m);
                }
                if(elementePtProdusNou.size()==0)
                {
                    eroareCitire("selectati produse existente pentru a creea un produs nou");
                }
                else
                    restaurant.addMenuItem(newItem);

            }

            model2.removeAllElements();
            frameCompositeProduct.dispose();
            frameCompositeProduct = view.butonCompositeProduct();
        }
    }

    /**
     * fereastra pentru butonul de delete
     */
    class butonDelete implements ActionListener{

        public void actionPerformed(ActionEvent e)
        {
             frameDelete=view.butonDelete();


        }
    }

    /**
     * butonul de delete din fereastra delete
     */
    class ButonDeleteItem implements ActionListener{
        private DefaultListModel<MenuItem> model1;
        private DefaultListModel<MenuItem> model2;
        public ButonDeleteItem(DefaultListModel<MenuItem> model1, DefaultListModel<MenuItem> model2)
        {
            this.model1=model1;
            this.model2=model2;
        }

        //se cauta toate produsele care contin elementele care trebuie sterse;
        //de ex daca avem rosii si salata care contine rosii
        //daca stergem rosiile, sa se stearga si salata.
        public LinkedList<MenuItem> getListaProduseDeSters()
        {

            LinkedList<MenuItem> elementeDeSters=new LinkedList<>();
            elementeDeSters = view.getElementeDreapta();
            LinkedList<MenuItem> produseDeSters=new LinkedList<>();

            if(elementeDeSters.size()!=0) {
                for (MenuItem m : elementeDeSters) {
                    for (MenuItem it : restaurant.getMeniu()) {
                        LinkedList<MenuItem> elemente = it.getItems();     // pentru fiecare element se cauta daca contine BaseProduct
                        //dupa care se sterge

                        if (elemente.contains(m)) {
                            produseDeSters.add(it);
                        }
                        if (m.getNume().equals(it.getNume()))          // deoarece CompositeProduct au doar nume si nu sunt produse singulare
                            produseDeSters.add(it);                    //acesta e un caz special(in cazul in care vrem sa stergem un produs composite
                        //acesta se sterge normal, deoarece base products raman neschimba
                    }
                }
            }

            return produseDeSters;

        }

        public void actionPerformed(ActionEvent e)
        {
            LinkedList<MenuItem> produseDeSters=getListaProduseDeSters();

            if(produseDeSters.size()==0)
            {
                eroareCitire("Alegeti macar un element pentru stergere");
            }
            else{
                for(MenuItem stergem:produseDeSters)
                {
                    restaurant.removeMenuItem(stergem);
                }
            }
            model2.removeAllElements();
            model1.removeAllElements();
            for(MenuItem m:restaurant.getMeniu())
                model1.addElement(m);

            frameDelete.dispose();
            frameDelete=view.butonDelete();



        }
    }

    /**
     * butonul de edit din admin
     */

    class ButonEdit implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            frameEdit = view.frameButonEdit();

        }

    }

    /**
     * butonul edit selected din fereastra de edit
     */
    class ButonEditSelected implements ActionListener{

        private DefaultListModel<MenuItem> model1;
        private JList<MenuItem> stanga;
        private DefaultListModel<MenuItem> model2;
        private JList<MenuItem> dreapta;
        private LinkedList<MenuItem> newElements;

        public ButonEditSelected(DefaultListModel<MenuItem> model1, DefaultListModel<MenuItem> model2, JList<MenuItem> stanga, JList<MenuItem> dreapta, LinkedList<MenuItem> newElements)
        {
            this.model1=model1;
            this.model2=model2;
            this.stanga=stanga;
            this.dreapta=dreapta;
            this.newElements=newElements;
        }


        public void actionPerformed(ActionEvent e)
        {
            String newName=view.getNewName();
            float newPrice=view.getNewPrice();
            String actualName=stanga.getSelectedValue().getNume();
            float actualPrice=stanga.getSelectedValue().computePrice();

            if(newName.isEmpty())
                newName=actualName;

            if(newPrice==0)
                newPrice=actualPrice;
            int i=0;

           restaurant.editItem(actualName,newName,newPrice,newElements);

            model2.removeAllElements();

            frameEdit.dispose();
            frameEdit=view.frameButonEdit();
        }


    }

    /**
     * butonul remove selected din fereastra de edit
     */
    class ButonRemoveElementDinCompositeProduct implements ActionListener{

        private LinkedList<MenuItem> newElements;
        private DefaultListModel<MenuItem> model2;
        private JList<MenuItem> dreapta;
        public ButonRemoveElementDinCompositeProduct(DefaultListModel<MenuItem> model2, JList<MenuItem> dreapta, LinkedList<MenuItem> newElements)
        {
            this.model2=model2;
            this.dreapta=dreapta;
            this.newElements=newElements;
        }

        public void actionPerformed(ActionEvent e)
        {
            newElements.remove(dreapta.getSelectedValue());
            model2.removeElement(dreapta.getSelectedValue());
        }
    }

    /**
     * butonul de move din fereastra edit
     */
    class ButonMovePtEdit implements ActionListener{

        private JList<MenuItem> stanga;
        private DefaultListModel<MenuItem> model2;
        LinkedList<MenuItem> newElements;

        public ButonMovePtEdit(JList<MenuItem> stanga, DefaultListModel<MenuItem> model2, LinkedList<MenuItem> newElements)
        {
            this.stanga=stanga;
            this.model2=model2;
            this.newElements=newElements;
        }
        public void actionPerformed(ActionEvent e)
        {
            for (MenuItem mem : stanga.getSelectedValue().getItems()) {
                model2.addElement(mem);
                newElements.add(mem);

            }
        }
    }



}




