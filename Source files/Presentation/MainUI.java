package Presentation;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;
import Business.*;
import Business.*;
import Data.RestaurantSerializator;

public class MainUI {

    private JButton butonAdmin=new JButton("Open Admin Window");
    private JButton butonWaiter=new JButton("Open Waiter Window");
    private JButton butonSave=new JButton("Save Restaurant");
    private IRestaurantProcessing restaurant;
    private String serial;


    public MainUI(String serial) throws Exception
    {
        this.serial=serial;

        restaurant =new RestaurantSerializator().deserialize(serial);

        JFrame frame=new JFrame("MainUi");
        frame.setLayout(new GridLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,400);

        butonAdmin.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                AdministratorView adm = new AdministratorView("Fereastra admin",(Restaurant)restaurant);
            }
        });

        butonWaiter.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                WaiterView waiter = new WaiterView("Fereastra waiter",(Restaurant)restaurant);
            }
        });

        butonSave.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {try {

                new RestaurantSerializator().serialize(restaurant);
                System.out.println(restaurant.getMeniu().size());
            }catch(Exception exc){};
            }
        });
        frame.add(butonAdmin);
        frame.add(butonWaiter);
        frame.add(butonSave);
        frame.setVisible(true);


    }

}
