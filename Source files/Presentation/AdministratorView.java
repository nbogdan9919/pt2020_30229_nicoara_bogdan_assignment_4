package Presentation;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.JList;
import Business.*;
import Business.MenuItem;

public class AdministratorView extends JFrame {

    private JFrame frame;
    private JPanel panel;
    private JButton add=new JButton("add Base Product");
    private JButton addCompositeItem=new JButton("add Composite Product");
    private JButton edit=new JButton("edit");
    private JButton delete=new JButton("delete");
    private JButton view=new JButton("view");
    private AdministratorController controller=new AdministratorController(this);

    //pt butonul de add base product
    JTextField numeTextBaseProduct=new JTextField(35);
    JTextField pretTextBaseProduct=new JTextField(35);

    JTextField numeCompositeProduct=new JTextField(35);
    JTextField editedName=new JTextField(35);
    JTextField editedPrice=new JTextField(35);
    DefaultListModel<MenuItem> model=new DefaultListModel<>();
    DefaultListModel<MenuItem> model2=new DefaultListModel<>();
    private Restaurant restaurant;



    public AdministratorView(String nume, Restaurant a)
    {this.restaurant=a;
        controller=new AdministratorController(this,a);
        frame=new JFrame(nume);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(500,500);
        panel=new JPanel();
        frame.setLayout(new GridLayout());
        panel.add(add);
        panel.add(addCompositeItem);
        panel.add(edit);
        panel.add(delete);
        panel.add(view);

        frame.add(panel);

        frame.setVisible(true);

        add.addActionListener(controller.new butonBaseProduct());
        addCompositeItem.addActionListener(controller.new butonCompositeProduct());
        view.addActionListener(controller.new butonView());
        delete.addActionListener(controller.new butonDelete());
        edit.addActionListener(controller.new ButonEdit());


    }
    public JFrame getFrame()
    {
        return frame;
    }


    public JFrame butonBaseProduct(){

        JFrame frame = new JFrame("buton base product");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(500, 500);
        JPanel panel = new JPanel();

        frame.setLayout(new GridLayout());

        JLabel numeBaseProduct = new JLabel("nume:");
        JLabel pretBaseProduct= new JLabel("pret:");
        JButton insert=new JButton("insert");
        insert.addActionListener(controller.new butonInsereazaBaseProduct());
        panel.add(numeBaseProduct);
        panel.add(numeTextBaseProduct);
        panel.add(pretBaseProduct);
        panel.add(pretTextBaseProduct);
        panel.add(insert);

        frame.add(panel);
        frame.setVisible(true);
        return frame;
    }

    public BaseProduct getBaseProduct()
    {
        String nume;
        float pret=0;
        if(numeTextBaseProduct.getText().trim().isEmpty()){
            nume="Eroare";
        }
        else{ nume=numeTextBaseProduct.getText();}

        if(pretTextBaseProduct.getText().trim().isEmpty())
        {
            nume="Eroare";

        }
        else {
            for(char a:pretTextBaseProduct.getText().toCharArray())
            {
                if(!Character.isDigit(a))
                    nume="Eroare";
            }

            if(nume!="Eroare")
                pret = Float.parseFloat(pretTextBaseProduct.getText());

        }
        return new BaseProduct(nume,pret);
    }

    public JFrame butonCompositeProduct()
    {

        JFrame frame = new JFrame("buton Composite product");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(new GridLayout());

        JPanel panel = panelButonCompositeProduct();

        frame.add(panel);

        frame.setVisible(true);
        return frame;
    }

    public JPanel panelButonCompositeProduct()
    {
        JPanel panel = new JPanel();
        LinkedList<MenuItem> meniu=controller.getMeniu();
        JList<MenuItem> stanga=new JList<>();
        JList<MenuItem>dreapta=new JList<>();
        model=new DefaultListModel<>();

        stanga.setModel(model);
        dreapta.setModel(model2);
        for(MenuItem m:meniu)
            model.addElement(m);

        JButton move=new JButton("move-->");
        JButton insert=new JButton("insert");
        insert.addActionListener( controller.new ButonInsereazaCompositeItem(model,model2));

        move.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                MenuItem m=stanga.getSelectedValue();
                if(m!=null)
                    model2.addElement(m);
                 m=dreapta.getSelectedValue();
                if(m!=null)
                model2.removeElement(dreapta.getSelectedValue());


            }});

        panel.add(stanga);
        panel.add(move);
        panel.add(dreapta);
        panel.add(new JLabel("nume:"));
        panel.add(numeCompositeProduct);
        panel.add(insert);
        return panel;
    }


    public String getNumeCompositeProduct()
    {
        return numeCompositeProduct.getText();
    }

    public LinkedList<MenuItem> getElementeDreapta()
    {
        LinkedList<MenuItem> m=new LinkedList<>();
        for(int i=0;i<model2.size();i++)
        {
            m.add(model2.getElementAt(i));

        }

        model2=new DefaultListModel<>();
        return m;
    }

    /**
     * frame buton delete
     * @return frame pentru buton delete
     */
    public JFrame butonDelete()
    {
        JFrame frame=new JFrame("Buton delete");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(new GridLayout());
        JPanel panel=panelButonDelete();

        frame.add(panel);
        frame.setVisible(true);
        return frame;
    }

    /**
     * panel buton delete
     * @return panel pentru buton delete
     */
    public JPanel panelButonDelete()
    {
        JPanel panel = new JPanel();
        LinkedList<MenuItem> meniu=controller.getMeniu();
        JList<MenuItem> stanga=new JList<>();
        JList<MenuItem>dreapta=new JList<>();
        model=new DefaultListModel<>();

        stanga.setModel(model);
        dreapta.setModel(model2);
        for(MenuItem m:meniu)
            model.addElement(m);

        JButton move=new JButton("move-->");
        JButton delete=new JButton("Delete");
        delete.addActionListener( controller.new ButonDeleteItem(model,model2));

        /**
         * butonul de move din delete
         */
        move.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                MenuItem m=stanga.getSelectedValue();
                        if(m!=null){
                model2.addElement(stanga.getSelectedValue());
                model.removeElement(stanga.getSelectedValue());}
        m=dreapta.getSelectedValue();
                        if(m!=null)
                        {
                model.addElement(dreapta.getSelectedValue());
                model2.removeElement(dreapta.getSelectedValue());}

            }});
        panel.add(stanga);
        panel.add(move);
        panel.add(dreapta);
        panel.add(delete);
        return panel;
    }

    public JFrame frameButonEdit()
    {
        JFrame frame = new JFrame("edit");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(new GridLayout());

        JPanel panel=panelButonEdit();

        frame.add(panel);
        frame.setVisible(true);
        return frame;
    }


    public JPanel panelButonEdit()
    {
        JPanel panel=new JPanel();
        LinkedList<MenuItem> meniu=controller.getMeniu();
        JList<MenuItem> stanga=new JList<>();
        JList<MenuItem>dreapta=new JList<>();
        model=new DefaultListModel<>();
        stanga.setModel(model);
        dreapta.setModel(model2);
        JButton editSelected=new JButton("edit Selected");
        JButton removeSelected=new JButton("remove selected");
        LinkedList<MenuItem> newElements=new LinkedList<MenuItem>();
        JButton move=new JButton("move-->");

        move.addActionListener(controller.new ButonMovePtEdit(stanga,model2,newElements));
        removeSelected.addActionListener(controller.new ButonRemoveElementDinCompositeProduct(model2,dreapta,newElements));
        editSelected.addActionListener(controller.new ButonEditSelected(model,model2,stanga,dreapta,newElements));

        for(MenuItem m:meniu)
            model.addElement(m);

        panel.add(stanga);
        panel.add(move);
        panel.add(removeSelected);
        panel.add(dreapta);
        panel.add(new JLabel("new name:"));
        panel.add(editedName);
        panel.add(new JLabel("new price"));
        panel.add(editedPrice);
        panel.add(editSelected);

        return panel;
    }

    /**
     *
     * @return string-ul din edit
     */
    public String getNewName()
    {
        String s="";
        s=s+  editedName.getText();

        return s;
    }

    /**
     *
     * @return numarul din  fereastra de edit
     */
    public float getNewPrice()
    {
        String s= editedPrice.getText();

        if(s.isEmpty())
        {
            s="0";
        }

        return Integer.parseInt(s);
    }

}

